import React, { Component } from 'react'
import { PostList } from './components/PostList'

const postsUrl = 'https://jsonplaceholder.typicode.com/posts?_limit=10';

class App extends Component {
    state = {
        posts: [],
    }

    componentDidMount() {
        fetch(postsUrl)
            .then(res => res.json())
            .then(data => this.setState({ posts: data }));
    }

    render() {
        return <div className="App" >
            <PostList posts={this.state.posts} />
        </div>
    }
}

export default App;
