import React from 'react'
import { PostItem } from './PostItem'

export const PostList = (props) => {
    const { posts } = props;

    return <div className="post-list">
        {
            posts.length ? <PostItem {...posts[0]} /> : null
        }
    </div>
}