import React from 'react';
import { CV } from './CV'

export const CV_List = () => {
    return <div>
        <h1>Hello from React</h1>
        <CV name='John Doe' prof='Web-Developer' spec='React'></CV>
        <CV name='Sara Smith' prof='Web-Developer' spec='Vue'></CV>
        <CV name='Brad Pete' prof='Web-Developer' spec='Angular'></CV>
    </div>
}