import React from 'react';
import ReactDOM from 'react-dom';
import { CV_List } from './components/CV_List'

function App() {
    return <CV_List />
}

ReactDOM.render(<App />, document.getElementById('root'));