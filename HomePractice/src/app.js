import React from 'react';
import ReactDOM from 'react-dom';
import { Posts } from './components/Posts';
import { MainContent } from './components/MainContent';
import 'bootstrap/dist/css/bootstrap.min';

function App() {
    return <div>
        <header>
            <div className="navbar navbar-dark bg-dark box-shadow">
                <strong className="navbar-brand d-flex align-items-center">Home Practice 1</strong>
            </div>
        </header>
        <main>
            <MainContent />
            <Posts />
        </main>
    </div>
}

ReactDOM.render(<App />, document.getElementById('root'));