import React, { useEffect, useState } from 'react';
import { PostItem } from './PostItem';
import 'bootstrap/dist/css/bootstrap.min.css';

const postUrl = 'https://jsonplaceholder.typicode.com/posts';

export const Posts = () => {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        fetch(postUrl).then(res => res.json()).then(data => setPosts(data));
    }, []);

    return <div className="album py-5 bg-light">
        <div className="container">
            <div className="row">
                {posts.map(post => <PostItem key={post.id} post={post} />)}
            </div>
        </div>
    </div>
}