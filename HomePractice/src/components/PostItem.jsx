import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'

export const PostItem = (props) => {
    const { post } = props;

    return <div className="col-md-4">
        <div className="card mb-4 box-shadow">
            <div className="card-img-top" style={{ height: 225, lineHeight: '225px', display: 'block', backgroundColor: 'GrayText', textAlign: '-webkit-center' }}>
                <div style={{ display: 'table-cell', verticalAlign: 'middle', height: 'inherit' }}>
                    <p style={{ color: 'white', verticalAlign: 'center', lineHeight: 'normal', width: 'inherit' }}>{post.title}</p>
                </div>
            </div>
            <div className="card-body">
                <p className="card-text">{post.body}</p>
                <div className="d-flex justify-content-end align-items-center">
                    <small className="text-muted">From UserId: {post.userId}</small>
                </div>
            </div>
        </div>
    </div>
}